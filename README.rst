.. note:: This project was originally written by K. Hansen. I took over
          maintenance in March of 2013. It was originally targeted at a
          single non-technical user running the script on Windows.

Python 2.7 is required to run this program. It can be downloaded from
http://www.python.org/download/.

After Python 2.7 is installed, the program can be run by double-clicking on
``craigslist.py``.

The following files must be in the same directory as ``craigslist.py``:

- ``history.csv``

  Log of old search hits. To reset searches (see all old hits again), delete
  this file.

- ``search.dat``

  Contains search information. Must be in the following format:

  - Line 1: search string, exactly how it would be entered on Craigslist
  - Line 2: minimum price
  - Line 3: maximum price
  - Line 4: List of categories, separated by spaces if more than one. Each one
    is three letters long, can be found by clicking on category in Craigslist,
    website URL will be ``...category=CAT/`` where CAT is the category.
  - Remaining Lines: Cities to search, one per line. Must be Craigslist city
    code. To find, go to Craigslist for that city. URL will be
    ``CITY.craigslist.org`` where CITY is the code needed. To find list of
    cities go to http://www.craigslist.org/about/sites.

After every search, a file called ``tmp_yy_mm_dd_hh_min.html`` will be created.
Open this file to view listings. These can then be deleted if desired.
