#!/usr/bin/env python
from __future__ import print_function

import argparse
from collections import OrderedDict
import csv
from datetime import datetime
import os
import random
import re
import sys
import time
import urllib


HISTORY_FILE = 'history.csv'


get_s = lambda n: '' if n == 1 else 's'


regex = '<div\s+class="content">(?P<records>.*?)(</h4>|</div>)'
regex = re.compile(regex, re.DOTALL)

record_regex = '<p.*?data-pid="(?P<listing_id>\d+)".*?>.*?</p>'
record_regex = re.compile(record_regex, re.DOTALL)

href_regex = re.compile('href="/', re.DOTALL)
href_template = 'href="http://{city}.craigslist.org/'


def get_listings(city, category, url, history):
    try:
        page = urllib.urlopen(url).read()
    except Exception as exc:
        raise FurnitureFinderException(
            'Problem reading page: {}'.format(url))

    listings = []
    listing_ids = []

    match = regex.search(page)
    if match:
        records = match.groupdict()['records']
        records = record_regex.finditer(records)
        for record in records:
            listing_id = record.groupdict()['listing_id']
            key = (city, category, listing_id)
            history.update_current(key)
            if key not in history:
                listings.append(record.group())
                history.add(key)

    return listings


def make_html(listings, filestart='tmp'):
    """

    ``listings`` is a mapping of city => [listings]

    """
    now = datetime.now()
    date = now.strftime('%Y_%m_%d_%H_%M_%S')
    filename = '{0}_{1}.html'.format(filestart, date)
    total = sum(len(ls) for ls in listings.values())
    new_listings_template = '<p>{n} new listing{s}</p>\n\n'
    with open(filename, 'w') as fp:
        fp.write(
            '<p>{}</p>\n\n'.format(now.strftime('%B %d, %Y at %I:%M%p')))
        fp.write(new_listings_template.format(n=total, s=get_s(total)))
        for city, city_listings in listings.items():
            city_total = len(city_listings)
            fp.write('<h2>{city}</h2>\n\n'.format(city=city))
            fp.write(
                new_listings_template
                .format(n=city_total, s=get_s(city_total)))
            href = href_template.format(city=city)
            listings = '\n\n'.join(city_listings)
            listings = href_regex.sub(href, listings)
            fp.write(listings)
    return filename


def make_url(city, search_terms='', min_price=0, max_price=9999999,
             category=''):
    return (
        'http://{city}.craigslist.org/search/{category}?'
        'query={query}&'
        'minAsk={minAsk}&'
        'maxAsk={maxAsk}&'
        'srchType=T'
    ).format(
        city=city,
        category=category,
        query='+'.join(search_terms),
        minAsk=min_price,
        maxAsk=max_price
    )


class History(object):

    fields = ('city', 'category', 'listing_id')

    def __init__(self, file_name=HISTORY_FILE):
        self.file_name = file_name
        self.listings = self.read()

        # Set of current listing keys (city, category, listing ID).
        # These are keys for *all* listings that are found during
        # a search. These keys are used to clean up the history file.
        # See `remove_expired()` for more details.
        self.current = set()

    def read(self):
        """Get listings history.

        Listings are stored in a CSV file with ``city``, ``category``,
        and ``listing_id`` fields.

        The listings will be returned as a map with this structure::

            city =>
                category =>
                    listing_id =>
                        city => city
                        category => category
                        listing_id => listing_id

        """
        listings = {}
        if os.path.exists(self.file_name):
            with open(self.file_name) as fp:
                reader = csv.DictReader(fp, self.fields)
                for row in reader:
                    city = row['city']
                    category = row['category']
                    listing_id = row['listing_id']
                    listings.setdefault(city, {})
                    listings[city].setdefault(category, {})
                    listings[city][category][listing_id] = row
        return listings

    def write(self, remove_expired=True):
        if remove_expired:
            self.remove_expired()
        with open(self.file_name, 'wb') as fp:
            writer = csv.DictWriter(fp, self.fields)
            for city in sorted(self.listings):
                for category in sorted(self.listings[city]):
                    for listing_id in sorted(self.listings[city][category]):
                        row = self.listings[city][category][listing_id]
                        writer.writerow(row)

    def remove_expired(self):
        """Remove expired listings.

        An expired listing meets the following criteria:

        - It *is* in the history file
        - It is *not* in the current set of listings
        - It *is* from a (city, category) currently being searched

        """
        pairs = set()
        for (city, category, _) in self.current:
            pairs.add((city, category))

        for (city, category) in pairs:
            for key in self.keys_for(city, category):
                if key not in self.current:
                    self.remove(key)

    def update_current(self, key):
        self.current.add(key)

    def add(self, key):
        city, category, listing_id = key
        self.listings.setdefault(city, {})
        self.listings[city].setdefault(category, {})
        self.listings[city][category][listing_id] = {
            'city': city,
            'category': category,
            'listing_id': listing_id,
        }

    def remove(self, key):
        city, category, listing_id = key
        city_listings = self.listings[city]
        category_listings = city_listings[category]
        del category_listings[listing_id]

    def keys_for(self, city, category):
        listings = self.listings[city]
        listings = listings[category]
        keys = []
        for listing_id in listings:
            keys.append((city, category, listing_id))
        return keys

    def __contains__(self, key):
        city, category, listing_id = key
        return (
            (city in self.listings) and
            (category in self.listings[city]) and
            (listing_id in self.listings[city][category])
        )

    def __iter__(self):
        for city in self.listings:
            for category in self.listings[city]:
                for listing_id in self.listings[city][category]:
                    yield (city, category, listing_id)


def do_search(cities, categories, search_terms, min_price=0, max_price=9999999,
              debug=False):
    print('Searching in categories: {}'.format(', '.join(categories)))
    print('Searching for: {}'.format(' '.join(search_terms)))
    print('Minimum price: {}'.format(min_price))
    print('Maximum price: {}'.format(max_price))
    print()

    try:
        history = History()
    except Exception as exc:
        raise FurnitureFinderException('Problem with history file.')

    new_listings_by_city = OrderedDict()
    total_new = 0

    for city in cities:
        print('Searching {city}...'.format(city=city), end=' ')
        new_listings = []
        for cat in categories:
            url = make_url(city, search_terms, min_price, max_price, cat)
            if debug:
                print(url, end=' ')

            listings = get_listings(city, cat, url, history)
            new_listings.extend(listings)

            # Throttle requests to try keep CL from doing it for us: pause
            # for somewhere between 1/8 and 1/4 of a second.
            seconds = random.randint(125, 250) * .001
            if debug:
                print('Pausing for {} seconds...'.format(seconds))
            time.sleep(seconds)

        if new_listings:
            new_listings_by_city[city] = new_listings
        num_new_listings = len(new_listings)
        total_new += num_new_listings
        print(
            '{} new listing{}'
            .format(num_new_listings, get_s(num_new_listings)))

    history.write()
    file_name = make_html(new_listings_by_city)
    s = get_s(total_new)

    if total_new > 0:
        print('{} new listing{} saved to {}'.format(total_new, s, file_name))
    else:
        print('No new listings found')


class FurnitureFinderException(Exception):

    def __init__(self, message):
        self.exc_info = sys.exc_info()
        super(FurnitureFinderException, self).__init__(message)


def handle_exc(exc_info=None, message=None, debug=False):
    """Show error message and exit.

    In normal mode, print the message and the exception, then exit with
    an error code. In debugging mode, print the message and re-raise the
    exception.

    """
    if exc_info is None:
        exc_info = sys.exc_info()
    exc_cls, exc, traceback = exc_info
    print('\n')
    sys.stdout.flush()
    print('An error was encountered:', end='\n\n', file=sys.stderr)
    if message is not None:
        print(message, end='\n\n', file=sys.stderr)
    if debug:
        raise exc_cls, exc, traceback
    else:
        print(str(exc), file=sys.stderr)
        exit(1)


def exit(status=0):
    try:
        raw_input('\nHit Enter key to exit...')
    except KeyboardInterrupt:
        print()
    sys.exit(status)


def parse_args(argv=None):
    try:
        search_terms, min_price, max_price, cats, cities = get_default_args()
    except Exception as exc:
        raise FurnitureFinderException('Problem with search file.')

    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--cities', nargs='*', default=cities)
    parser.add_argument('-a', '--categories', nargs='*', default=cats)
    parser.add_argument(
        '-s', '--search-terms', nargs='*', default=search_terms)
    parser.add_argument('-n', '--min-price', type=int, default=min_price)
    parser.add_argument('-x', '--max-price', type=int, default=max_price)
    parser.add_argument('--clean', action='store_true', default=False)
    parser.add_argument('-d', '--debug', action='store_true', default=False)

    args = parser.parse_args(argv)

    if args.debug:
        args.cities = args.cities[:2]

    return args


def get_default_args():
    with open('search.dat') as fp:
        search_terms = fp.readline().strip().split()
        min_price = fp.readline().strip()
        max_price = fp.readline().strip()
        cats = fp.readline().strip().split()
    with open('cities.dat') as cities_fp:
        cities = [c.strip() for c in cities_fp.readlines()]
    return search_terms, min_price, max_price, cats, cities


def main(argv=None):
    debug = True
    try:
        args = parse_args(argv)

        if args.clean:
            print('Cleaning...')
            if os.path.exists(HISTORY_FILE):
                os.remove(HISTORY_FILE)
                print('Removed {}'.format(HISTORY_FILE))
            for f in os.listdir('.'):
                if f.startswith('tmp_'):
                    os.remove(f)
                    print('Removed {}'.format(f))

        do_search(
            args.cities,
            args.categories,
            args.search_terms,
            args.min_price,
            args.max_price,
            args.debug,
        )
    except KeyboardInterrupt:
        sys.exit()
    except FurnitureFinderException as exc:
        handle_exc(exc.exc_info, exc.message, debug)
    except Exception as exc:
        handle_exc(debug=debug)
    else:
        exit()


if __name__ == '__main__':
    main()
